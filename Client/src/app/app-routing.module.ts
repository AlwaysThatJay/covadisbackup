import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './users/login/login.component';
import { ProductsComponent } from './products/products.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LocationsComponent } from './locations/locations.component';
import { AddlocationsComponent } from './locations/addlocations/addlocations.component';
import { OrdersComponent } from './orders/orders.component';
import { RequestProductComponent } from './products/requestproduct/requestproduct.component';
import { RequestedProductsComponent } from './products/requestedproducts/requestedproducts.component';
import { AcceptrequestedproductComponent } from './products/acceptrequestedproduct/acceptrequestedproduct.component';
import { SettingsComponent } from './users/settings/settings.component';
import { UsersComponent } from './users/users.component';
import { Error401Component } from './errors/error401/error401.component';
import { Error404Component } from './errors/error404/error404.component';
import { SpecificuserComponent } from './users/specificuser/specificuser.component';
import { NgModule } from '@angular/core';
import { ConfirmpasswordComponent } from './users/confirmpassword/confirmpassword.component';
import { Role } from './API/_models/roles';
import { AuthGuard } from './API/_guards/auth.guard';
import { SpecificlocationComponent } from './locations/specificlocation/specificlocation.component';
import { TimComponent } from './tim/tim.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },

  {
    path: 'products',
    component: ProductsComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanSeeProducts }
  },
  {
    path: 'specificlocation/:id',
    component: SpecificlocationComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanEditLocation }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent

  },
  {
    path: 'locations', //done
    component: LocationsComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanSeeLocations }
  },
  {
    path: 'addlocations',
    component: AddlocationsComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanEditLocation }
  },
  {
    path: 'orders',
    component: OrdersComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanSeeOrders }
  },
  {
    path: 'requestproduct',
    component: RequestProductComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanRequestProduct }
  },
  {
    path: 'requestedproducts',
    component: RequestedProductsComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanSeeRequests }
  },
  {
    path: 'acceptrequestedproduct/:id',
    component: AcceptrequestedproductComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanAcceptRequests }
  },
  {
    path: 'settings/:id',
    component: SettingsComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanEditUser }
  },
  {
    path: 'specificuser/:id', //done
    component: SpecificuserComponent,
  },
  {
    path: 'users', //done
    component: UsersComponent,
    canActivate: [AuthGuard],
    data: { roles: Role.CanSeeUsers }
  },
  {
    path: 'error401',
    component: Error401Component
  },
  {
    path: 'specificuser',
    component: SpecificuserComponent
  },
  {
    path: 'confirmpassword/:guid',
    component: ConfirmpasswordComponent

  },
  {
    path: 'tim',
    component: TimComponent
  },
  {
    path: '**',
    component: Error404Component
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
