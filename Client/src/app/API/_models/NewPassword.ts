export class NewPassword {
    constructor(
        public UserId: number,
        public OldPassword: string,
        public NewPassword: string,
        public NewPasswordConfirm: string
    ) { }
}
