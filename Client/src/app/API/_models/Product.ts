export class Product {
    constructor(
        public productId: number,
        public Name: string,
        public Image: string,
        public Shop: number,
        public Durability: boolean
    ) { }
}
