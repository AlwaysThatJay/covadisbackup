export class LocationProduct {
    constructor(
        public departmentId: number,
        public productId: number
    ) { }
}
