export class RegisterUser {
    constructor(
        public UserName: string,
        public FirstName: string,
        public LastName: string
    ) { }
}
