export class OrderProduct {
    constructor(
        public DepartmentId: number,
        public UserId: number,
        public Id: number,
        public Amount: number,
        public Urgent: boolean
    ) { }
}
