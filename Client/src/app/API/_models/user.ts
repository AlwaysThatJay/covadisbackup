export class User {
    constructor(
        public userId: number,
        public username: string,
        public password: string,
        public firstName: string,
        public lastName: string,
        public permissions: string[],
        public token?: string,
    ) { }
}
