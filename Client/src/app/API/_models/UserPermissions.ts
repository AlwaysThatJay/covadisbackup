export class UserPermissions {
    constructor(
        public Permission: string,
        public Owns: boolean
    ) { }
}
