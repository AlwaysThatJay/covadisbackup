export class LocationUser {
    constructor(
        public departmentId: number,
        public userId: number
    ) { }
}
