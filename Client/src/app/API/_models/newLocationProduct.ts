export class NewLocationProduct {
    constructor(
        public RequestId: number,
        public Name: string,
        public Image: string,
        public Shop: string,
        public Link: string,
        public Durability: boolean,
        public Location: number,
        public ProductId: number
    ) { }
}
