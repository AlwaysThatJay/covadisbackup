export class RequestProduct {
    constructor(
        public ProductName: string,
        public Motivation: string,
        public Location: number,
        public RequestedBy: number
    ) { }
}
