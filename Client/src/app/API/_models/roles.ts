export enum Role {
    CanSeeProducts = 'CanSeeProducts', // +
    CanOrderProduct = 'CanOrderProduct', // +
    CanRequestProduct = 'CanRequestProduct', // +
    CanSeeRequests = 'CanSeeRequests', // +
    CanAcceptRequests = 'CanAcceptRequests', // +
    CanSeeOrders = 'CanSeeOrders', // +
    CanAcceptOrders = 'CanAcceptOrders', // +
    CanDeliverOrder = 'CanDeliverOrder', // +
    CanReceiveOrder = 'CanReceiveOrder', // +
    CanSeeLocations = 'CanSeeLocations', // +
    CanCreateLocation = 'CanCreateLocation',
    CanEditLocation = 'CanEditLocation', // +
    CanDeleteLocation = 'CanDeleteLocation',
    CanSeeUsers = 'CanSeeUsers', // +
    CanRegisterUser = 'CanRegisterUser', // +
    CanEditUser = 'CanEditUser', // +
    CanEditUserPermissions = 'CanEditUserPermissions', // +
    CanDeleteUser = 'CanDeleteUser',
    CanEditProduct = 'CanEditProduct', // +
    CanDeleteProduct = 'CanDeleteProduct', // +
    CanCreateNewProduct = 'CanCreateNewProduct', // +
    CanSeeDashboard = 'CanSeeDashboard',
}
