export class PasswordConfirm {
    constructor(
        public guid: string,
        public password: string,
        public passwordConfirm: string
    ) { }
}
