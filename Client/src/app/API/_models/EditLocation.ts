export class EditLocation {
    constructor(
        public LocationId: number,
        public LocationName: string
    ) { }
}
