export class Location {
    constructor(
        public DepartmentId: number,
        public DepartmentName: string,
    ) { }
}
