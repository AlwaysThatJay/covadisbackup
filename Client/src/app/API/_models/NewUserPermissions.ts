export class NewUserPermissions {
    constructor(
        public UserId: number,
        public NewPermissions: string[]
    ) { }
}
