import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '../_services/authentication.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser) {
            // check if route is restricted by role
            // console.log('Perms' + currentUser.permissions.toString());
            // console.log(route.data.roles);
            for (let index = 0; index < currentUser.permissions.length; index++) {
                // console.log(currentUser.permissions[index]);
                if (route.data.roles === currentUser.permissions[index]) {
                    return true;
                }
            }
            if (route.data.roles !== currentUser.permissions) {
                // role not authorised so redirect to home page
                this.router.navigate(['/error401']);
                return false;
            } else {
                this.router.navigate(['/error401' + route.url]);
                return true;
            }
        }
    }
}
