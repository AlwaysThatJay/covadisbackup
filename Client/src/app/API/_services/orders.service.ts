import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OrderProduct } from '../_models/OrderProduct';
import { OrderedProduct } from '../_models/OrderedProduct';

@Injectable()
export class OrdersService {

    constructor(private http: HttpClient) { }

    OrderProduct(product: OrderProduct) {
        return this.http.post('https://localhost:44394/api/orders/CreateNewOrder', product);
    }
    GetAllOrders() {
        return this.http.get('https://localhost:44394/api/orders/GetAllOrders');
    }
    OrderedProduct(orderedProduct: OrderedProduct) {
        return this.http.put('https://localhost:44394/api/orders/OrderedProduct', orderedProduct);
    }
    IsDelivered(orderedProduct: OrderedProduct) {
        return this.http.put('https://localhost:44394/api/orders/OrderedProduct', orderedProduct);
    }
    IsReceived(orderedProduct: OrderedProduct) {
        return this.http.put('https://localhost:44394/api/orders/OrderedProduct', orderedProduct);
    }
}
