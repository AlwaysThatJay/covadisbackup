import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class LocationsService {


    constructor(private http: HttpClient) {

    }
    GetAllLocations() {
        return this.http.get('https://localhost:44394/api/Department/GetAllDepartments');
    }
    GetSpecificLocation(id: number) {
        return this.http.get('https://localhost:44394/api/department//GetSpecificDepartment/' + id);
    }
    AddLocation(location) {
        return this.http.post('https://localhost:44394/api/department/NewLocation', location);
    }
    GetSpecificUserDepartments(id: number) {
        return this.http.get('https://localhost:44394/api/users/GetAllUserSpecificDepartments/' + id);
    }
    EditLocation(location) {
        return this.http.put('https://localhost:44394/api/department/EditLocation', location);
    }
}
