import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterUser } from '../_models/RegisterUser';
import { stringify } from 'querystring';


@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get('https://localhost:44394/api/users/GetAllUsers');
    }
    CreateUser(user: RegisterUser) {
        return this.http.post('https://localhost:44394/api/users/RegisterUser', user);
    }
    SpecificUser(id: number) {
        return this.http.get('https://localhost:44394/api/users/GetSpecificUser/' + id);
    }
    GetPermission() {
        return this.http.get('https://localhost:44394/api/users/GetAllPermissions');
    }
    GetAllUserPermissions(id) {
        return this.http.get('https://localhost:44394/api/users/GetAllUserPermissions/' + id);
    }
    UpdateUserPermission(permissions) {
        return this.http.post('https://localhost:44394/api/users/UpdateUserPermissions/', permissions);
    }
    ChangePassword(password) {
        return this.http.post('https://localhost:44394/api/users/UpdatePassword/', password);
    }
    GetAllLocationUsers(id) {
        return this.http.get('https://localhost:44394/api/users/GetAllLocationUsers/' + id);
    }
    RemoveUserLocation(id) {
        return this.http.delete('https://localhost:44394/api/users/DeleteUserLocation/' + id);
    }
    NewLocationUser(user) {
        return this.http.post('https://localhost:44394/api/users/NewLocationUser/', user);
    }
    ConfirmPassword(password) {
        return this.http.post('https://localhost:44394/api/users/ConfirmPassword/', password);
    }
    ForgotPassword(email) {
        return this.http.post('https://localhost:44394/api/users/ForgotPassword/', email);
    }
    DeleteUser(id) {
        return this.http.delete('https://localhost:44394/api/users/DeleteUser/' + id);
    }
}
