import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NewLocationProduct } from '../_models/NewLocationProduct';

@Injectable()
export class ProductsService {

    constructor(private http: HttpClient) {

    }
    GetAllProducts() {
        return this.http.get('https://localhost:44394/api/values/GetAllProducts');
    }
    GetSpecificProduct(id: number) {
        return this.http.get('https://localhost:44394/api/values/GetSpecificProduct/' + id);
    }
    GetSpecificLocationProducts(id: number) {
        return this.http.get('https://localhost:44394/api/values/GetSpecificLocationProducts/' + id);
    }
    NewLocationProduct(locationproduct: NewLocationProduct) {
        return this.http.post('https://localhost:44394/api/values/productDepartment/NewLocationProduct/', locationproduct);
    }
    GetAllLocationProducts() {
        return this.http.get('https://localhost:44394/api/values/GetAllLocationProducts/');
    }
    LocationProduct(product) {
        return this.http.post('https://localhost:44394/api/request/AcceptRequest', product);
    }
    RemoveProductLocation(dproductId: number) {
        return this.http.delete('https://localhost:44394/api/values/DeleteLocationProduct/' + dproductId);
    }
    SpecificLocationProduct(id) {
        return this.http.get('https://localhost:44394/api/values/GetSpecificLocationProduct/' + id);
    }
}
