import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestProduct } from '../_models/RequestProduct';

@Injectable()
export class RequestService {

    constructor(private http: HttpClient) { }

    RequestProduct(requestProduct: RequestProduct) {
        return this.http.post('https://localhost:44394/api/request/NewRequest', requestProduct);
    }

    GetAllRequests() {
        return this.http.get('https://localhost:44394/api/request/GetAllRequests');
    }

    GetSpecificRequest(id: number) {
        return this.http.get('https://localhost:44394/api/request/GetSpecificRequest/' + id);
    }

    DeleteRequest(id: number) {
        return this.http.delete('https://localhost:44394/api/request/DeleteRequest/' + id);
    }
}
