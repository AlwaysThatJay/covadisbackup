import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterPipeUsers'
  })
  export class FilterPipeUsers implements PipeTransform {
    transform(items: any[], searchToken: string): any[] {
      if (searchToken == null) {
        searchToken = '';
      }

      if (searchToken === undefined) { return items; }
      if (!searchToken) { return items; }

      searchToken = searchToken.toLowerCase();
      return items.filter(elem => elem.username.toLowerCase().indexOf(searchToken.toLowerCase()) > -1);
    }

}