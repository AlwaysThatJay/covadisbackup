import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificuserComponent } from './specificuser.component';

describe('SpecificuserComponent', () => {
  let component: SpecificuserComponent;
  let fixture: ComponentFixture<SpecificuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
