import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/API/_models/User';
import { UserPermissions } from 'src/app/API/_models/UserPermissions';
import { UserService } from 'src/app/API/_services/user.service';
import { NewUserPermissions } from 'src/app/API/_models/NewUserPermissions';

@Component({
  selector: 'app-specificuser',
  templateUrl: './specificuser.component.html',
  styleUrls: ['./specificuser.component.scss']
})
export class SpecificuserComponent implements OnInit {
  user: User;
  id;
  permissions;
  userPermissions;
  newPermissions: UserPermissions[] = [];
  allPermission = false;
  constructor(private userService: UserService,
    private toastr: ToastrService,
    private route: ActivatedRoute) { }
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.SpecificUser();
    this.getPermissions();
    this.GetAllUserPermissions();
    this.Allpermissions();
  }
  getPermissions() {
    this.userService.GetPermission().subscribe(data => {
      this.permissions = data;
    });
  }
  SpecificUser() {
    this.userService.SpecificUser(this.id).subscribe(data => {
      this.user = <User><unknown>data;
    });
  }
  GetAllUserPermissions() {
    this.userService.GetAllUserPermissions(this.id).subscribe(data => {
      this.userPermissions = data;
      this.userService.GetPermission().subscribe(pdata => {
        this.permissions = pdata;
        const permissionNames = [];
        for (let index = 0; index < this.permissions.length; index++) {
          permissionNames[index] = this.permissions[index].permission;
        }
        for (let index = 0; index < this.permissions.length; index++) {
          this.newPermissions.push({ Permission: this.permissions[index].permission, Owns: false });
        }
        this.userPermissions.forEach(element => {
          for (let index = 0; index < this.newPermissions.length; index++) {
            if (this.newPermissions[index].Permission === element.permissionId.permission) {
              this.newPermissions[index].Owns = true;
            }
          }
          if (this.permissions.length === this.newPermissions.length) {
            this.allPermission = true;
          }
        });
      });
    });
  }
  EditUserPermissions(permission) {
    for (let index = 0; index < NewUserPermissions.length; index++) {
      if (this.newPermissions[index].Permission === permission) {
        if (this.newPermissions[index].Owns === true) {
          this.newPermissions[index].Owns = false;
        } else {
          this.newPermissions[index].Owns = true;
        }
      }
    }
    if (this.permissions.length === this.newPermissions.filter(elem => elem.Owns).length && this.permissions.length !== 0) {
      this.allPermission = true;
    } else {
      this.allPermission = false;
    }
  }
  UpdateUserPermissions() {
    const permissionArray: string[] = [];
    for (let index = 0; index < this.newPermissions.length; index++) {
      if (this.newPermissions[index].Owns === true) {
        permissionArray[index] = this.newPermissions[index].Permission.toString();
      }
    }
    const permissions = new NewUserPermissions(this.id, permissionArray);
    this.userService.UpdateUserPermission(permissions).subscribe(() => {
      this.GetAllUserPermissions();
      location.reload();
      this.toastr.success('De permissies zijn geupdate!', 'Geupdate!');
    });
  }
  Allpermissions() {
    for (let index = 0; index < this.newPermissions.length; index++) {
      if (this.allPermission === true) {
        this.newPermissions[index].Owns = false;
      } else {
        this.newPermissions[index].Owns = true;
      }
    }
  }
}
