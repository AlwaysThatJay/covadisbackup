import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/API/_services/authentication.service';
import { loginUser } from 'src/app/API/_models/LoginUser';
import { delay } from 'q';
import { UserService } from 'src/app/API/_services/user.service';
import { RegisterUser } from 'src/app/API/_models/RegisterUser';
import { ToastrService } from 'ngx-toastr';


@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    forgotPassword = false;
    email = '';

    constructor(
        private toastr: ToastrService,
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = 'dashboard';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }


    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }


        this.loading = true;
        const loginuser = new loginUser(this.f.username.value, this.f.password.value);
        this.authenticationService.login(loginuser)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                message => {
                    console.log(message);
                    this.error = message.Message;
                    this.loading = false;
                    this.toastr.error(message.Message);

                });
    }

    ForgotPassword() {
        const account = new RegisterUser(this.email, '', '');
        this.userService.ForgotPassword(account).subscribe(data => {
            this.forgotPassword = false;
        });
        this.toastr.success('Er is een email naar uw Email-addres Verstuurd!', 'Verstuurd!');

    }
}
