import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/API/_services/user.service';
import { PasswordConfirm } from 'src/app/API/_models/PasswordConfirm';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-confirmpassword',
  templateUrl: './confirmpassword.component.html',
  styleUrls: ['./confirmpassword.component.scss']
})
export class ConfirmpasswordComponent implements OnInit {

  constructor(private route: ActivatedRoute, private toastr: ToastrService, private router: Router, private userService: UserService) { }

  guid;
  returnUrl: string;
  newPassword: PasswordConfirm = new PasswordConfirm('', '', '');

  ngOnInit() {
    this.guid = this.route.snapshot.paramMap.get('guid');
    this.returnUrl = 'login';
  }

  ConfirmRegister() {
    this.newPassword.guid = this.guid;
    this.userService.ConfirmPassword(this.newPassword).subscribe(data => {
    });
    this.toastr.success('Wachtwoord succesvol gewijzigd!', 'Succes!');
    this.router.navigate([this.returnUrl]);
  }

}
