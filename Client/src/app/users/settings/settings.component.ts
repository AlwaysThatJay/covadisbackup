import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/API/_services/user.service';
import { NewPassword } from 'src/app/API/_models/NewPassword';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(private userService: UserService,
    private toastr: ToastrService,
    private router: Router) { }

  user;
  changePassword = false;
  newPassword: NewPassword = new NewPassword(0, '', '', '');
  error = '';
  message = '';

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.newPassword.UserId = this.user.userId;
  }

  PasswordChanged() {
    this.userService.ChangePassword(this.newPassword).subscribe(data => {
      this.changePassword = false;
      this.toastr.success('Wachtwoord succesvol gewijzigd!', 'Succes!');
      this.newPassword = new NewPassword(this.user.userId, '', '', '');
    },
      error => {
        this.error = error;
      });
  }
}
