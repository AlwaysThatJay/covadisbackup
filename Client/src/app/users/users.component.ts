import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../API/_services/user.service';
import { FilterPipeUsers } from '../Filters/filterpipeUsers';
import { RegisterUser } from '../API/_models/RegisterUser';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private userService: UserService,
    private routing: Router,
    private toastr: ToastrService) {
  }

  users;
  searchToken: string;
  registerUser: RegisterUser = new RegisterUser('', '', '');

  ngOnInit() {
    this.GetAllUsers();
  }

  GetAllUsers() {
    this.userService.getAll().subscribe(data => {
      this.users = data;
    });
  }

  SpecificUser(id) {
    this.routing.navigateByUrl('specificuser/' + id);
  }

  DeleteUser(id) {
    this.userService.DeleteUser(id).subscribe(data => {
      this.GetAllUsers();
    });
  }

  CreateAccount() {
    this.userService.CreateUser(this.registerUser).subscribe(data => {
      this.routing.navigateByUrl('users');
    },
      message => {
        this.toastr.error(message.Message);

      });
  }
}
