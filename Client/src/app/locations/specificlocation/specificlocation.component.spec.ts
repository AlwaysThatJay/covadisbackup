import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificlocationComponent } from './specificlocation.component';

describe('SpecificlocationComponent', () => {
  let component: SpecificlocationComponent;
  let fixture: ComponentFixture<SpecificlocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificlocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificlocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
