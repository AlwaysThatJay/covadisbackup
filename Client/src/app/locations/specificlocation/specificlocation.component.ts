import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EditLocation } from 'src/app/API/_models/EditLocation';
import { NewLocationProduct } from 'src/app/API/_models/NewLocationProduct';
import { LocationsService } from 'src/app/API/_services/location.services';
import { ProductsService } from 'src/app/API/_services/products.services';
import { UserService } from 'src/app/API/_services/user.service';
import { LocationUser } from 'src/app/API/_models/LocationUser';
import { Product } from 'src/app/API/_models/Product';
import { LocationProduct } from 'src/app/API/_models/LocationProduct';

@Component({
  selector: 'app-specificlocation',
  templateUrl: './specificlocation.component.html',
  styleUrls: ['./specificlocation.component.scss']
})

export class SpecificlocationComponent implements OnInit {

  private id;
  location;
  existinglocation: EditLocation = new EditLocation(this.id, '');
  error: string;
  locationproducts;
  products = [];
  product = 0;
  newProduct: NewLocationProduct = new NewLocationProduct(0, '', '', '', '', false, 0, 0);
  pPopup;
  uPopup;
  locationUsers;
  users;
  user = 0;

  constructor(private locationService: LocationsService,
    private route: ActivatedRoute,
    private routing: Router,
    private productService: ProductsService,
    private userService: UserService) { }

  ngOnInit() {
    this.SpecificLocation();
    this.GetAllLocationUsers();
    this.GetAllUsers();
    this.GetAllProducts();
  }
  SpecificLocation() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.locationService.GetSpecificLocation(this.id).subscribe(data => {
      this.location = data;
      this.existinglocation.LocationName = this.location.departmentName;
    });
  }

  EditLocation() {
    this.existinglocation.LocationId = this.id;
    this.locationService.EditLocation(this.existinglocation).subscribe(
      data => {

      },
      error => {
        this.error = error;
      });
  }

  GetAllProducts() {
    this.productService.GetAllProducts().subscribe(products => {
      this.productService.GetSpecificLocationProducts(this.id).subscribe(data => {
        this.locationproducts = <LocationProduct[]>data;

        const allproducts = <Product[]>products;

        allproducts.forEach(element => {
          if (this.locationproducts.find(e => e.productId.productId === element.productId) === undefined) {
            this.products.push(element);
          }
        });
      });
    });
  }

  GetAllLocationUsers() {
    this.userService.GetAllLocationUsers(this.id).subscribe(data => {
      this.locationUsers = data;
    });
  }

  NewLocationProduct() {
    this.newProduct.Location = this.id;
    this.newProduct.ProductId = this.product;
    this.productService.LocationProduct(this.newProduct).subscribe(() => this.GetAllProducts());
  }

  RemoveProduct(dproductId: number) {
    this.productService.RemoveProductLocation(dproductId).subscribe(() => {
      this.GetAllProducts();
    });
  }

  RemoveUser(duserId) {
    this.userService.RemoveUserLocation(duserId).subscribe(() => {
      this.GetAllLocationUsers();
    });
  }

  GetAllUsers() {
    this.userService.getAll().subscribe(data => {
      this.users = data;
    });
  }

  NewLocationUser() {
    const newUser: LocationUser = new LocationUser(this.id, this.user);
    this.userService.NewLocationUser(newUser).subscribe(() => {
      this.GetAllLocationUsers();
      this.uPopup = false;
    });
  }

  IsInLocation(id: number) {
    if (this.locationproducts.find(e => e.productId.productId === id) !== undefined) {
      return true;
    } else {
      return false;
    }
  }
}
