import { Component, OnInit } from '@angular/core';
import { AddLocation } from '../../API/_models/AddLocation';
import { Router } from '@angular/router';
import { LocationsService } from 'src/app/API/_services/location.services';

@Component({
  selector: 'app-addlocations',
  templateUrl: './addlocations.component.html',
  styleUrls: ['./addlocations.component.scss']
})
export class AddlocationsComponent implements OnInit {

  constructor(private locationService: LocationsService, private routing: Router) { }

  location: AddLocation = new AddLocation('');
  error: string;

  ngOnInit() {
  }

  AddLocation() {
    this.locationService.AddLocation(this.location).subscribe(
      data => {
        this.routing.navigateByUrl('locations');
      },
      error => {
        this.error = error;
      });
  }
}
