import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddlocationsComponent } from './addlocations.component';

describe('AddlocationsComponent', () => {
  let component: AddlocationsComponent;
  let fixture: ComponentFixture<AddlocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddlocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddlocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
