import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationsService } from '../API/_services/location.services';
import { AddLocation } from '../API/_models/AddLocation';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  constructor(
    private locationService: LocationsService,
    private routing: Router,
    private route: ActivatedRoute) {
  }

  public i = false;
  location: AddLocation = new AddLocation('');
  error: string;

  public locations;

  public id;
  searchToken: string;

  ngOnInit() {
    this.GetAllLocations();
  }

  GetAllLocations() {
    this.locationService.GetAllLocations().subscribe(data => {
      this.locations = data;
    });
  }

  public SpecificLocation(id: number) {
    this.routing.navigateByUrl('specificlocation/' + id);
  }
  Modelopen() {
    this.i = true;
  }


  AddLocation() {
    this.locationService.AddLocation(this.location).subscribe(
      data => {
        this.GetAllLocations();
      },
      error => {
        this.error = error;
      });
  }
}
