import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProductsComponent } from './products/products.component';
import { OrdersComponent } from './orders/orders.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { UsersComponent } from './users/users.component';
import { IgxTabsModule } from 'igniteui-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './users/login/login.component';
import { LocationsComponent } from './locations/locations.component';
import { AddlocationsComponent } from './locations/addlocations/addlocations.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RequestProductComponent } from './products/requestproduct/requestproduct.component';
import { RequestedProductsComponent } from './products/requestedproducts/requestedproducts.component';
import { AcceptrequestedproductComponent } from './products/acceptrequestedproduct/acceptrequestedproduct.component';
import { Error404Component } from './errors/error404/error404.component';
import { SettingsComponent } from './users/settings/settings.component';
import { SpecificlocationComponent } from './locations/specificlocation/specificlocation.component';
import { Error401Component } from './errors/error401/error401.component';
import { SpecificuserComponent } from './users/specificuser/specificuser.component';
import { ToastrModule } from 'ngx-toastr';
import { ProductsService } from './API/_services/products.services';
import { LocationsService } from './API/_services/location.services';
import { OrdersService } from './API/_services/orders.service';
import { RequestService } from './API/_services/requestproduct.service';
import { JwtInterceptor } from './API/_helpers/jwt.interceptor';
import { ErrorInterceptor } from './API/_helpers/error.interceptor';
import { ConfirmpasswordComponent } from './users/confirmpassword/confirmpassword.component';
import { FilterPipeDep } from './Filters/filterpipeDepartment';
import { FilterPipeUsers } from './Filters/filterpipeUsers';
import { TimComponent } from './tim/tim.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    LocationsComponent,
    AddlocationsComponent,
    DashboardComponent,
    NavbarComponent,
    OrdersComponent,
    RequestProductComponent,
    RequestedProductsComponent,
    AcceptrequestedproductComponent,
    Error404Component,
    SettingsComponent,
    UsersComponent,
    SpecificlocationComponent,
    Error401Component,
    SpecificuserComponent,
    ConfirmpasswordComponent,
    FilterPipeDep,
    FilterPipeUsers,
    TimComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    IgxTabsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    ProductsService,
    LocationsService,
    OrdersService,
    RequestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
