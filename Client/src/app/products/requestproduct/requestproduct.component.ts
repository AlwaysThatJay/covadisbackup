import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from 'src/app/API/_models/User';
import { RequestProduct } from 'src/app/API/_models/RequestProduct';
import { ProductsService } from 'src/app/API/_services/products.services';
import { LocationsService } from 'src/app/API/_services/location.services';
import { RequestService } from 'src/app/API/_services/requestproduct.service';

@Component({
  selector: 'app-request-product',
  templateUrl: './requestproduct.component.html',
  styleUrls: ['./requestproduct.component.scss']
})
export class RequestProductComponent implements OnInit {

  location;
  userLocations;
  user: User;
  requestProduct: RequestProduct = new RequestProduct('', '', 0, 0);

  constructor(private productService: ProductsService,
    private locationService: LocationsService,
    private toastr: ToastrService,
    private requestService: RequestService,
    private routing: Router) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.requestProduct.RequestedBy = this.user.userId;
    this.GetUserLocations();
  }

  showSuccess() {
    this.toastr.success('Dit product is aangevraagd', 'Aangevraagd!');
  }

  RequestProduct() {
    this.requestService.RequestProduct(this.requestProduct).subscribe();
    this.routing.navigateByUrl('products');
  }

  GetUserLocations() {
    this.locationService.GetSpecificUserDepartments(this.user.userId).subscribe(arg => {
      this.userLocations = arg;
    });
  }
}
