import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationsService } from '../API/_services/location.services';
import { ProductsService } from '../API/_services/products.services';
import { UserService } from '../API/_services/user.service';
import { ToastrService } from 'ngx-toastr';
import { OrdersService } from '../API/_services/orders.service';
import { OrderProduct } from '../API/_models/OrderProduct';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products;
  locationProducts;
  id;
  locations;
  user;
  orderProduct;
  amount;
  urgent = false;
  orders;

  constructor(private productService: ProductsService,
    private locationService: LocationsService,
    private routing: Router,
    private orderService: OrdersService) {
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.GetAllProducts();
    this.GetAllLocationProducts();
    this.GetAllLocations();
    this.GetAllOrders();
  }

  SpecificProduct(id: number) {
    this.routing.navigateByUrl('specificproduct/' + id);
  }

  GetAllProducts() {
    this.productService.GetAllProducts().subscribe(data => {
      this.products = data;
    });
  }

  GetAllLocationProducts() {
    this.productService.GetAllLocationProducts().subscribe(data => {
      this.locationProducts = data;
    });
  }

  GetAllLocations() {
    this.locationService.GetSpecificUserDepartments(this.user.userId).subscribe(data => {
      this.locations = data;
    });
  }

  OrderProduct(id) {
    this.productService.SpecificLocationProduct(id).subscribe(data => {
      this.orderProduct = data;
    });
  }

  CreateOrder() {
    const product = new OrderProduct(this.orderProduct.departmentId.departmentId,
    this.user.userId, this.orderProduct.productId.productId, this.amount, this.urgent);
    this.urgent = false;
    this.amount = '';
    this.orderService.OrderProduct(product).subscribe(data => {
      this.GetAllOrders();
    });
  }

  GetAllOrders() {
    this.orderService.GetAllOrders().subscribe(data => {
      this.orders = data;
    });
  }

  IsOrdered(id: number) {
    console.log(this.orders.find(e => e.productId.productId === id));
    if (this.orders.find(e => e.productId.productId === id) !== undefined) {
      return this.orders.find(e => e.productId.productId === id).statusId.statusName;
    } else {
      return null;
    }
  }
}
