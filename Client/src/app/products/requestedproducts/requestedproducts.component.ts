import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RequestService } from 'src/app/API/_services/requestproduct.service';

@Component({
  selector: 'app-requestedproducts',
  templateUrl: './requestedproducts.component.html',
  styleUrls: ['./requestedproducts.component.scss']
})
export class RequestedProductsComponent implements OnInit {

  constructor(private requestedProductsService: RequestService, private toastr: ToastrService, private routing: Router) { }

  requests;

  ngOnInit() {
    this.GetAllRequests();
  }

  showSuccess() {
    this.toastr.success('De permissies zijn geupdate!', 'Geupdate!');
  }

  GetAllRequests() {
    this.requestedProductsService.GetAllRequests().subscribe(data => {
      this.requests = data;
    });
  }
  GoToRequest(id) {
    this.routing.navigateByUrl('acceptrequestedproduct/' + id);
  }

}
