import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptrequestedproductComponent } from './acceptrequestedproduct.component';

describe('AcceptrequestedproductComponent', () => {
  let component: AcceptrequestedproductComponent;
  let fixture: ComponentFixture<AcceptrequestedproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptrequestedproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptrequestedproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
