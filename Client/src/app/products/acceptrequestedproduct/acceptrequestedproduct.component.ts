import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestService } from 'src/app/API/_services/requestproduct.service';
import { ProductsService } from 'src/app/API/_services/products.services';
import { NewLocationProduct } from 'src/app/API/_models/NewLocationProduct';
import { RequestProduct } from 'src/app/API/_models/RequestProduct';

@Component({
  selector: 'app-acceptrequestedproduct',
  templateUrl: './acceptrequestedproduct.component.html',
  styleUrls: ['./acceptrequestedproduct.component.scss']
})
export class AcceptrequestedproductComponent implements OnInit {

  constructor(private requestService: RequestService,
    private productService: ProductsService,
    private route: ActivatedRoute,
    private router: Router) { }

  request;
  id;
  newProduct: NewLocationProduct = new NewLocationProduct(0, '', '', '', '', false, 0, 0);

  ngOnInit() {
    this.GetRequest();
    this.newProduct.RequestId = this.id;
    console.log(this.newProduct);
  }

  GetRequest() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.requestService.GetSpecificRequest(this.id).subscribe(data => {
      this.request = data;
    });
  }

  Deny() {
    this.requestService.DeleteRequest(this.id).subscribe(data => {
      this.router.navigateByUrl('requestedproducts');
    });
  }

  Accept() {
    console.log(this.request);
    this.newProduct.Name = this.request.productName;
    this.newProduct.Location = this.request.location.departmentId;
  }
  NewProductCreated() {
    this.productService.LocationProduct(this.newProduct).subscribe(data => {
      this.router.navigateByUrl('requestedproducts');
    });
  }
}
