import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../API/_services/orders.service';
import { LocationsService } from '../API/_services/location.services';
import { ProductsService } from '../API/_services/products.services';
import { RequestService } from '../API/_services/requestproduct.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private orderService: OrdersService,
    private locationService: LocationsService,
    private productService: ProductsService,
    private requestProducts: RequestService) { }

  orders;
  openOrders;
  locations;
  products;
  requestedProducts;

  ngOnInit() {
    this.GetData();
  }
  GetData() {
    this.orderService.GetAllOrders().subscribe(data => {
      this.orders = data;
      this.openOrders = this.orders.filter(elem => elem.statusId.statusName === 'Openstaand');
      console.log(this.openOrders);
    });
    this.locationService.GetAllLocations().subscribe(data => {
      this.locations = data;
    });
    this.productService.GetAllProducts().subscribe(data => {
      this.products = data;
    });
    this.requestProducts.GetAllRequests().subscribe(data => {
      this.requestedProducts = data;
    });

  }

}

