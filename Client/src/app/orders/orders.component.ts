import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../API/_services/orders.service';
import { OrderedProduct } from '../API/_models/OrderedProduct';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  public orders;
  private user;

  constructor(private orderService: OrdersService) { }

  ngOnInit() {
    this.GetOrders();
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  GetOrders() {
    this.orderService.GetAllOrders().subscribe(data => {
      this.orders = data;
    });
  }
  ConfirmOrder(id) {
    const product = new OrderedProduct(id, this.user.userId);
    this.orderService.OrderedProduct(product).subscribe(result => {
      this.GetOrders();
    });
  }
  Delivered(id) {
    const product = new OrderedProduct(id, this.user.userId);
    this.orderService.IsDelivered(product).subscribe(result => {
      this.GetOrders();
    });
  }
  Received(id) {
    const product = new OrderedProduct(id, this.user.userId);
    this.orderService.IsReceived(product).subscribe(result => {
      this.GetOrders();
    });
  }
}
