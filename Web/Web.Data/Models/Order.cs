﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Web.Data.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        public User UserId { get; set; }
        public Department DepartmentId { get; set; }
        public Product ProductId { get; set; }
        public Status StatusId { get; set; }
        public int OrderAmount { get; set; }
        public bool IsUrgent { get; set; }
    }
}
