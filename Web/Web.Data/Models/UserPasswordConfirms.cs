﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Web.Data.Models
{
    public class UserPasswordConfirms
    {
        [Key]
        public int UserPasswordConfirmsId { get; set; }
        public User UserId { get; set; }
        public Guid Guid { get; set; }
    }
}
