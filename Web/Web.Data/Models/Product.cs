﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Data.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public string ProductLink { get; set; }
        public Shop ProductShop { get; set; }
        public bool ProductShortDurability { get; set; }
    }
}
