﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Web.Data.Models
{
    public class ProductDepartment
    {
        [Key]
        public int ProductDepartmentId { get; set; }
        public Product ProductId { get; set; }
        public Department DepartmentId { get; set; }
    }
}
