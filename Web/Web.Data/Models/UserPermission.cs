﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Web.Data.Models
{
    public class UserPermission
    {
        [Key]
        public int UserPermissionId { get; set; }
        public Permissions PermissionId { get; set; }
        public User UserId { get; set; }
    }
}
