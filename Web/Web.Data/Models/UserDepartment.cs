﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Web.Data.Models
{
    public class UserDepartment
    {
        [Key]
        public int UserDepartmentId { get; set; }
        public User UserId { get; set; }
        public Department DepartmentId { get; set; }
    }
}
