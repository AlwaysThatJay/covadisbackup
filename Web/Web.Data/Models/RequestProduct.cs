﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Data.Models
{
    public class RequestProduct
    {
        public int RequestProductId { get; set; }
        public string ProductName { get; set; }
        public string Motivation { get; set; }
        public User RequestedBy { get; set; }
        public Department Location { get; set; }
    }
}
