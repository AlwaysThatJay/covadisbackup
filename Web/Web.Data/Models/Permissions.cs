﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Web.Data.Models
{
    public class Permissions
    {
        [Key]
        public int PermissionId { get; set; }
        public string Permission { get; set; }
    }
}