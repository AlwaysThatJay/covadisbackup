﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Data.Models
{
    public class Shop
    {
        public int ShopId { get; set; }
        public string ShopName { get; set; }
        public string ShopURL { get; set; }
        public bool Inactive { get; set; }
    }
}
