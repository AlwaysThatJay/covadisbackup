﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class ik : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RequestedByUserId",
                table: "TblRequestProduct",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TblRequestProduct_RequestedByUserId",
                table: "TblRequestProduct",
                column: "RequestedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TblRequestProduct_TblUsers_RequestedByUserId",
                table: "TblRequestProduct",
                column: "RequestedByUserId",
                principalTable: "TblUsers",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblRequestProduct_TblUsers_RequestedByUserId",
                table: "TblRequestProduct");

            migrationBuilder.DropIndex(
                name: "IX_TblRequestProduct_RequestedByUserId",
                table: "TblRequestProduct");

            migrationBuilder.DropColumn(
                name: "RequestedByUserId",
                table: "TblRequestProduct");
        }
    }
}
