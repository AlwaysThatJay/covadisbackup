﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class AddedOrderBy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderedByUserId",
                table: "TblOrders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TblOrders_OrderedByUserId",
                table: "TblOrders",
                column: "OrderedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TblOrders_TblUsers_OrderedByUserId",
                table: "TblOrders",
                column: "OrderedByUserId",
                principalTable: "TblUsers",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblOrders_TblUsers_OrderedByUserId",
                table: "TblOrders");

            migrationBuilder.DropIndex(
                name: "IX_TblOrders_OrderedByUserId",
                table: "TblOrders");

            migrationBuilder.DropColumn(
                name: "OrderedByUserId",
                table: "TblOrders");
        }
    }
}
