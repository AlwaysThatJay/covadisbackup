﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class Addedinactivex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblOrders_TblStatus_StatusId",
                table: "TblOrders");

            migrationBuilder.RenameColumn(
                name: "StatusId",
                table: "TblOrders",
                newName: "StatusId1");

            migrationBuilder.RenameIndex(
                name: "IX_TblOrders_StatusId",
                table: "TblOrders",
                newName: "IX_TblOrders_StatusId1");

            migrationBuilder.AddForeignKey(
                name: "FK_TblOrders_TblStatus_StatusId1",
                table: "TblOrders",
                column: "StatusId1",
                principalTable: "TblStatus",
                principalColumn: "StatusId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblOrders_TblStatus_StatusId1",
                table: "TblOrders");

            migrationBuilder.RenameColumn(
                name: "StatusId1",
                table: "TblOrders",
                newName: "StatusId");

            migrationBuilder.RenameIndex(
                name: "IX_TblOrders_StatusId1",
                table: "TblOrders",
                newName: "IX_TblOrders_StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_TblOrders_TblStatus_StatusId",
                table: "TblOrders",
                column: "StatusId",
                principalTable: "TblStatus",
                principalColumn: "StatusId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
