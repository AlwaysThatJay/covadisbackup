﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class Addedlocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LocationDepartmentId",
                table: "TblRequestProduct",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TblRequestProduct_LocationDepartmentId",
                table: "TblRequestProduct",
                column: "LocationDepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_TblRequestProduct_TblDepartment_LocationDepartmentId",
                table: "TblRequestProduct",
                column: "LocationDepartmentId",
                principalTable: "TblDepartment",
                principalColumn: "DepartmentId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblRequestProduct_TblDepartment_LocationDepartmentId",
                table: "TblRequestProduct");

            migrationBuilder.DropIndex(
                name: "IX_TblRequestProduct_LocationDepartmentId",
                table: "TblRequestProduct");

            migrationBuilder.DropColumn(
                name: "LocationDepartmentId",
                table: "TblRequestProduct");
        }
    }
}
