﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblDepartment",
                columns: table => new
                {
                    DepartmentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DepartmentName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblDepartment", x => x.DepartmentId);
                });

            migrationBuilder.CreateTable(
                name: "TblRole",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblRole", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "TblShop",
                columns: table => new
                {
                    ShopId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShopName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblShop", x => x.ShopId);
                });

            migrationBuilder.CreateTable(
                name: "TblUsers",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    RoleId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblUsers", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_TblUsers_TblRole_RoleId1",
                        column: x => x.RoleId1,
                        principalTable: "TblRole",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TblProducts",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductName = table.Column<string>(nullable: true),
                    ProductImage = table.Column<string>(nullable: true),
                    ProductShopShopId = table.Column<int>(nullable: true),
                    ProductShortDurability = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblProducts", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_TblProducts_TblShop_ProductShopShopId",
                        column: x => x.ProductShopShopId,
                        principalTable: "TblShop",
                        principalColumn: "ShopId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TblUserDepartment",
                columns: table => new
                {
                    UserDepartmentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId1 = table.Column<int>(nullable: true),
                    DepartmentId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblUserDepartment", x => x.UserDepartmentId);
                    table.ForeignKey(
                        name: "FK_TblUserDepartment_TblDepartment_DepartmentId1",
                        column: x => x.DepartmentId1,
                        principalTable: "TblDepartment",
                        principalColumn: "DepartmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TblUserDepartment_TblUsers_UserId1",
                        column: x => x.UserId1,
                        principalTable: "TblUsers",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TblOrders",
                columns: table => new
                {
                    OrderId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId1 = table.Column<int>(nullable: true),
                    DepartmentId1 = table.Column<int>(nullable: true),
                    ProductId1 = table.Column<int>(nullable: true),
                    IsUrgent = table.Column<bool>(nullable: false),
                    IsDelivered = table.Column<bool>(nullable: false),
                    OrderAmount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblOrders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_TblOrders_TblDepartment_DepartmentId1",
                        column: x => x.DepartmentId1,
                        principalTable: "TblDepartment",
                        principalColumn: "DepartmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TblOrders_TblProducts_ProductId1",
                        column: x => x.ProductId1,
                        principalTable: "TblProducts",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TblOrders_TblUsers_UserId1",
                        column: x => x.UserId1,
                        principalTable: "TblUsers",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TblProductDepartment",
                columns: table => new
                {
                    ProductDepartmentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId1 = table.Column<int>(nullable: true),
                    DepartmentId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblProductDepartment", x => x.ProductDepartmentId);
                    table.ForeignKey(
                        name: "FK_TblProductDepartment_TblDepartment_DepartmentId1",
                        column: x => x.DepartmentId1,
                        principalTable: "TblDepartment",
                        principalColumn: "DepartmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TblProductDepartment_TblProducts_ProductId1",
                        column: x => x.ProductId1,
                        principalTable: "TblProducts",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TblOrders_DepartmentId1",
                table: "TblOrders",
                column: "DepartmentId1");

            migrationBuilder.CreateIndex(
                name: "IX_TblOrders_ProductId1",
                table: "TblOrders",
                column: "ProductId1");

            migrationBuilder.CreateIndex(
                name: "IX_TblOrders_UserId1",
                table: "TblOrders",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_TblProductDepartment_DepartmentId1",
                table: "TblProductDepartment",
                column: "DepartmentId1");

            migrationBuilder.CreateIndex(
                name: "IX_TblProductDepartment_ProductId1",
                table: "TblProductDepartment",
                column: "ProductId1");

            migrationBuilder.CreateIndex(
                name: "IX_TblProducts_ProductShopShopId",
                table: "TblProducts",
                column: "ProductShopShopId");

            migrationBuilder.CreateIndex(
                name: "IX_TblUserDepartment_DepartmentId1",
                table: "TblUserDepartment",
                column: "DepartmentId1");

            migrationBuilder.CreateIndex(
                name: "IX_TblUserDepartment_UserId1",
                table: "TblUserDepartment",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_TblUsers_RoleId1",
                table: "TblUsers",
                column: "RoleId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblOrders");

            migrationBuilder.DropTable(
                name: "TblProductDepartment");

            migrationBuilder.DropTable(
                name: "TblUserDepartment");

            migrationBuilder.DropTable(
                name: "TblProducts");

            migrationBuilder.DropTable(
                name: "TblDepartment");

            migrationBuilder.DropTable(
                name: "TblUsers");

            migrationBuilder.DropTable(
                name: "TblShop");

            migrationBuilder.DropTable(
                name: "TblRole");
        }
    }
}
