﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class lit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductShortDurability",
                table: "TblRequestProduct");

            migrationBuilder.RenameColumn(
                name: "ProductImage",
                table: "TblRequestProduct",
                newName: "Motivation");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Motivation",
                table: "TblRequestProduct",
                newName: "ProductImage");

            migrationBuilder.AddColumn<bool>(
                name: "ProductShortDurability",
                table: "TblRequestProduct",
                nullable: false,
                defaultValue: false);
        }
    }
}
