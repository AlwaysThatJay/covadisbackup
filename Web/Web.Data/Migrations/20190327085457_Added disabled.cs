﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class Addeddisabled : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblOrders_TblUsers_OrderedByUserId",
                table: "TblOrders");

            migrationBuilder.DropColumn(
                name: "IsDelivered",
                table: "TblOrders");

            migrationBuilder.DropColumn(
                name: "IsReceived",
                table: "TblOrders");

            migrationBuilder.RenameColumn(
                name: "OrderedByUserId",
                table: "TblOrders",
                newName: "StatusId");

            migrationBuilder.RenameIndex(
                name: "IX_TblOrders_OrderedByUserId",
                table: "TblOrders",
                newName: "IX_TblOrders_StatusId");

            migrationBuilder.AddColumn<bool>(
                name: "Inactive",
                table: "TblUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Inactive",
                table: "TblShop",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Inactive",
                table: "TblDepartment",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "TblStatus",
                columns: table => new
                {
                    StatusId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StatusName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblStatus", x => x.StatusId);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_TblOrders_TblStatus_StatusId",
                table: "TblOrders",
                column: "StatusId",
                principalTable: "TblStatus",
                principalColumn: "StatusId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblOrders_TblStatus_StatusId",
                table: "TblOrders");

            migrationBuilder.DropTable(
                name: "TblStatus");

            migrationBuilder.DropColumn(
                name: "Inactive",
                table: "TblUsers");

            migrationBuilder.DropColumn(
                name: "Inactive",
                table: "TblShop");

            migrationBuilder.DropColumn(
                name: "Inactive",
                table: "TblDepartment");

            migrationBuilder.RenameColumn(
                name: "StatusId",
                table: "TblOrders",
                newName: "OrderedByUserId");

            migrationBuilder.RenameIndex(
                name: "IX_TblOrders_StatusId",
                table: "TblOrders",
                newName: "IX_TblOrders_OrderedByUserId");

            migrationBuilder.AddColumn<bool>(
                name: "IsDelivered",
                table: "TblOrders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsReceived",
                table: "TblOrders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_TblOrders_TblUsers_OrderedByUserId",
                table: "TblOrders",
                column: "OrderedByUserId",
                principalTable: "TblUsers",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
