﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class _02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblUsers_TblRole_RoleId1",
                table: "TblUsers");

            migrationBuilder.DropTable(
                name: "TblRole");

            migrationBuilder.DropIndex(
                name: "IX_TblUsers_RoleId1",
                table: "TblUsers");

            migrationBuilder.DropColumn(
                name: "RoleId1",
                table: "TblUsers");

            migrationBuilder.AddColumn<string>(
                name: "ShopURL",
                table: "TblShop",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductLink",
                table: "TblProducts",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsReceived",
                table: "TblOrders",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShopURL",
                table: "TblShop");

            migrationBuilder.DropColumn(
                name: "ProductLink",
                table: "TblProducts");

            migrationBuilder.DropColumn(
                name: "IsReceived",
                table: "TblOrders");

            migrationBuilder.AddColumn<int>(
                name: "RoleId1",
                table: "TblUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TblRole",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblRole", x => x.RoleId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TblUsers_RoleId1",
                table: "TblUsers",
                column: "RoleId1");

            migrationBuilder.AddForeignKey(
                name: "FK_TblUsers_TblRole_RoleId1",
                table: "TblUsers",
                column: "RoleId1",
                principalTable: "TblRole",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
