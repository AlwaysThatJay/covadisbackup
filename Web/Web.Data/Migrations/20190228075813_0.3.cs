﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class _03 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblPermissions",
                columns: table => new
                {
                    PermissionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Permission = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblPermissions", x => x.PermissionId);
                });

            migrationBuilder.CreateTable(
                name: "TblUserPermission",
                columns: table => new
                {
                    UserPermissionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PermissionId1 = table.Column<int>(nullable: true),
                    UserId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblUserPermission", x => x.UserPermissionId);
                    table.ForeignKey(
                        name: "FK_TblUserPermission_TblPermissions_PermissionId1",
                        column: x => x.PermissionId1,
                        principalTable: "TblPermissions",
                        principalColumn: "PermissionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TblUserPermission_TblUsers_UserId1",
                        column: x => x.UserId1,
                        principalTable: "TblUsers",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TblUserPermission_PermissionId1",
                table: "TblUserPermission",
                column: "PermissionId1");

            migrationBuilder.CreateIndex(
                name: "IX_TblUserPermission_UserId1",
                table: "TblUserPermission",
                column: "UserId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblUserPermission");

            migrationBuilder.DropTable(
                name: "TblPermissions");
        }
    }
}
