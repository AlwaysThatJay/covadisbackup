﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Data.Migrations
{
    public partial class plsworkpls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "TblUsers",
                newName: "Username");

            migrationBuilder.RenameColumn(
                name: "PasswordHash",
                table: "TblUsers",
                newName: "Token");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "TblUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "TblUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "TblUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "TblUsers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "TblUsers");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "TblUsers");

            migrationBuilder.RenameColumn(
                name: "Username",
                table: "TblUsers",
                newName: "UserName");

            migrationBuilder.RenameColumn(
                name: "Token",
                table: "TblUsers",
                newName: "PasswordHash");
        }
    }
}
