﻿using Microsoft.EntityFrameworkCore;
using System;
using Web.Data.Models;

namespace Web.Data
{
    public class CovadisDbContext : DbContext
    {
        public CovadisDbContext(DbContextOptions<CovadisDbContext> options) : base(options)
        {
        }

        public DbSet<User> TblUsers { get; set; }
        public DbSet<Department> TblDepartment { get; set; }
        public DbSet<Order> TblOrders { get; set; }
        public DbSet<Product> TblProducts { get; set; }
        public DbSet<ProductDepartment> TblProductDepartment { get; set; }
        public DbSet<Shop> TblShop { get; set; }
        public DbSet<UserDepartment> TblUserDepartment { get; set; }
        public DbSet<Permissions> TblPermissions { get; set; }
        public DbSet<UserPermission> TblUserPermission { get; set; }
        public DbSet<RequestProduct> TblRequestProduct { get; set; }
        public DbSet<Status> TblStatus { get; set; }
        public DbSet<UserPasswordConfirms> TblUserPasswordConfirms { get; set; }
    }
}

