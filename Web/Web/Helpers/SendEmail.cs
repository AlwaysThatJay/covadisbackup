﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Web.Data.Models;

namespace Web.Helpers
{
    public class SendEmail
    {
        public void SendMail(User account, string subject, Guid guid)
        {
            var confirmLink = "http://localhost:4200/confirmpassword/" + guid;

            if (account == null)
            {
                throw new Exception("Dit account bestaat niet");
            }

            MailMessage mail = new MailMessage("covadisfacility@gmail.com", account.Username);
            SmtpClient client = new SmtpClient
            {
                Port = 587,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Host = "in-v3.mailjet.com",
                Credentials = new NetworkCredential("10e12fa815d8c142339771d1f561a86e", "1835b9af4975d81ebc96baee3e655c94")
            };

            if (subject == "ConfirmPassword")
            {
                string html = System.IO.File.ReadAllText("./Content/confirmpassword.html");
                html = html.Replace("+name+", account.FirstName + " " + account.LastName);
                html = html.Replace("+confirmLink+", confirmLink);
                mail.Subject = "Bevestig uw aanmelding";
                mail.Body = html;
                mail.IsBodyHtml = true;
                client.Send(mail);
            }
            else if (subject == "ForgotPassword")
            {
                string html = System.IO.File.ReadAllText("./Content/changepassword.html");
                html = html.Replace("+name+", account.FirstName + " " + account.LastName);
                html = html.Replace("+confirmLink+", confirmLink);
                mail.Subject = "Verander uw wachtwoord";
                mail.Body = html;
                mail.IsBodyHtml = true;
                client.Send(mail);
            }
        }
    }
}
