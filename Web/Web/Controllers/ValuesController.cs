﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.Data;
using Web.Data.Models;
using Web.Models;

namespace Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private CovadisDbContext _Dbc;
        public ValuesController(CovadisDbContext Dbc)
        {
            _Dbc = Dbc;
        }

        // GET api/values
        [HttpGet]
        [Authorize(Roles = "CanSeeProducts")]
        public IActionResult GetAllProducts()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            return Ok(_Dbc.TblProducts.Include(b => b.ProductShop));
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [Authorize(Roles = "CanEditProduct")]
        public IActionResult GetSpecificProduct(int id)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            return Ok(_Dbc.TblProducts.Include(b => b.ProductShop).Where(p => p.ProductId == id));
        }

        [HttpGet("{id}")]
        public IActionResult GetSpecificLocationProducts(int id)
        {
            return Ok(_Dbc.TblProductDepartment.Where(x => x.DepartmentId == _Dbc.TblDepartment.Where(d => d.DepartmentId == id).FirstOrDefault()).Include(x => x.DepartmentId).Include(x => x.ProductId));
        }

        [HttpGet]
        public IActionResult GetAllLocationProducts()
        {
            return Ok(_Dbc.TblProductDepartment.Include(x => x.DepartmentId).Include(p => p.ProductId));
        }

        [HttpPost]
        [Authorize(Roles = "CanCreateNewProduct")]
        public void NewLocationProduct(NewLocationProduct newLocationProduct)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            var locationProduct = new ProductDepartment
            {
                DepartmentId = _Dbc.TblDepartment.Where(x => x.DepartmentId == newLocationProduct.DepartmentId).FirstOrDefault(),
                ProductId = _Dbc.TblProducts.Where(x => x.ProductId == newLocationProduct.ProductId).FirstOrDefault()
            };

            _Dbc.TblProductDepartment.Add(locationProduct);
            _Dbc.SaveChanges();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "CanDeleteProduct")]
        public void DeleteLocationProduct(int id)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            _Dbc.TblProductDepartment.Remove(_Dbc.TblProductDepartment.Where(x => x.ProductDepartmentId == id).FirstOrDefault());
            _Dbc.SaveChanges();
        }

        [HttpGet("{id}")]
        public IActionResult GetSpecificLocationProduct(int id)
        {
            return Ok(_Dbc.TblProductDepartment.Where(x => x.ProductDepartmentId == id).Include(x => x.ProductId).Include(x => x.DepartmentId).FirstOrDefault());
        }
    }
}
