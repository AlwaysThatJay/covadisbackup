﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Models;
using Web.Services;

namespace Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RequestController : ControllerBase
    {

        private readonly IRequestService _service;
        public RequestController(IRequestService service)
        {
            _service = service;
        }

        // GET: api/Request
        [HttpGet]
        [Authorize(Roles = "CanSeeRequests")]
        public IActionResult GetAllRequests()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            return Ok(_service.GetAllRequests());
        }

        // GET: api/Request/5
        [HttpGet("{id}")]
        public IActionResult GetSpecificRequest(int id)
        {
            return Ok(_service.GetSpecificRequest(id));
        }

        // POST: api/Request
        [HttpPost]
        [Authorize(Roles = "CanRequestProduct")]
        public IActionResult NewRequest(Models.RequestProduct product)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _service.RequestNewProduct(product);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }

        [HttpPost]
        [Authorize(Roles = "CanAcceptRequests")]
        public IActionResult AcceptRequest(AcceptRequest product)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _service.AcceptRequest(product);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "CanAcceptRequests")]
        public IActionResult DeleteRequest(int id)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _service.DenyRequest(id);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
    }
}
