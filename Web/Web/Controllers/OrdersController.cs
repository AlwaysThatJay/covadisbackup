﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Data;
using Web.Models;
using Web.Services;

namespace Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _service;

        public OrdersController(IOrderService service)
        {
            _service = service;
        }
        //Haalt alle orders op
        [HttpGet]
        [Authorize(Roles = "CanSeeOrders")]
        public IActionResult GetAllOrders()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            return Ok(_service.GetAllOrders());
        }

        // POST: api/Orders
        [HttpPost]
        [Authorize(Roles = "CanOrderProduct")]
        public IActionResult CreateNewOrder(OrderMore product)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _service.CreateNewOrder(product);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }

        // PUT: api/Orders/5
        [HttpPut]
        [Authorize(Roles = "CanDeliverOrder, CanReceiveOrder, CanDeliverOrder  ")]
        public IActionResult OrderedProduct(OrderedProduct product)
        {
            try
            {
                _service.OrderedProduct(product);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
    }
}
