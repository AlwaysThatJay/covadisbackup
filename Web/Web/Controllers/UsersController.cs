﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Data;
using Web.Models;
using Web.Services;

namespace Web.Controllers
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService, CovadisDbContext Dbc)
        {
            _userService = userService;
        }
        //Alle users worden naar de api door verstuurd.
        [HttpGet]
        [Authorize(Roles = "CanSeeUsers")]
        public IActionResult GetAllUsers()
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                return Ok(_userService.GetAllUsers());
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Stuurt een bepaalde user door naar de api door middel van een Id
        [HttpGet("{id}")]
        [Authorize(Roles = "CanEditUser")]
        public IActionResult GetSpecificUser(int id)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                return Ok(_userService.GetSpecificUser(id));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Er wordt een Ok status terug gestuurd naar de applicatie als de user correct heeft ingelogd.
        [AllowAnonymous]
        [HttpPost]

        public IActionResult Authenticate(LoginUser loginuser)
        {
            try
            {
                return Ok(_userService.Authenticate(loginuser.userName, loginuser.password));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Als een User met succes geregistreerd is wordt er een OK status gegeven.
        [HttpPost]
        [Authorize(Roles = "CanRegisterUser")]
        public IActionResult RegisterUser(RegisterUser registerUser)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _userService.RegisterUser(registerUser);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Alle permissions worden gestuurd naar de api.
        [HttpGet]
        [Authorize(Roles = "CanEditUserPermissions")]
        public IActionResult GetAllPermissions()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            return Ok(_userService.GetAllPermissions());
        }
        //Alle permissions worden voor bepaalde gebruikers opgehaald
        [HttpGet("{id}")]
        [Authorize(Roles = "CanEditUserPermissions")]
        public IActionResult GetAllUserPermissions(int id)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                return Ok(_userService.GetAllUserPermissions(id));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //als permissions worden toegevoegt aan de gebruiker geeft hij een OK status terug
        [HttpPost]
        [Authorize(Roles = "CanEditUserPermissions")]
        public IActionResult UpdateUserPermissions(NewUserPermissions permissions)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _userService.UpdateUserPermissions(permissions);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Als een wachtwoord succesvol is aangepast word er een Ok status gegeven.
        [HttpPost]
        public IActionResult UpdatePassword(ChangePassword u)
        {
            try
            {
                _userService.ChangePassword(u);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Stuurt een "oke status" als je het wachtwoord hebt ingevuld.
        [HttpPost]
        public IActionResult ConfirmPassword(ConfirmPassword password)
        {
            try
            {
                _userService.ConfirmPassword(password);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Haalt alle locaties op.
        [HttpGet]
        public IActionResult GetAllUserDepartments()
        {
            return Ok(_userService.GetAllUserDepartments());
        }
        //Kijkt naar welke locatie een gebruiker heeft.
        [HttpGet("{id}")]
        public IActionResult GetAllUserSpecificDepartments(int id)
        {
            try
            {
                return Ok(_userService.GetAllUserSpecificDepartments(id));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Alle users die aan een specifieke locatie zijn gekoppeld worden opgehaald.
        [HttpGet("{id}")]
        [Authorize(Roles = "CanSeeLocations")]
        public IActionResult GetAllLocationUsers(int id)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                return Ok(_userService.GetAllLocationUsers(id));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Als er een locatie gekoppeld word aan een user, word dat naar de API gestuurd.
        [HttpPost]
        [Authorize(Roles = "CanEditLocation")]
        public IActionResult NewLocationUser(NewLocationUser newUser)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _userService.NewLocationUser(newUser);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Het verwijderen van de koppeling tussen de user en zijn/haar locatie.
        [Authorize(Roles = "CanEditLocation")]
        [HttpDelete("{id}")]
        public IActionResult DeleteUserLocation(int id)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _userService.DeleteUserLocation(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
        //Als iemand een password wil aanpassen word er een "oke status" gestuurd.
        [HttpPost]
        public IActionResult ForgotPassword(RegisterUser reg)
        {
            _userService.ForgotPassword(reg.UserName);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                _userService.DeleteUser(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }

    }
}