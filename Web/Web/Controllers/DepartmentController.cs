﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Data;
using Web.Models;
using Web.Services;

namespace Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentService _service;

        public DepartmentController(IDepartmentService service)
        {
            _service = service;
        }

        // GET: api/Department
        [HttpGet]
        [Authorize(Roles = "CanSeeLocations")]
        public IActionResult GetAllDepartments()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            return Ok(_service.GetAllDepartments());
        }

        // GET: api/Department/5
        [HttpGet("{id}")]
        [Authorize(Roles = "CanSeeLocations")]
        public IActionResult GetSpecificDepartment(int id)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;

            }
            return Ok(_service.GetSpecificDepartment(id));
        }

        // POST: api/Department
        [HttpPost]
        [Authorize(Roles = "CanCreateLocation")]
        public IActionResult NewLocation(EditLocation addlocation)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                var location = new AddLocation
                {
                    LocationName = addlocation.LocationName
                };

                _service.NewLocation(location);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest( new { message = ex });
            }
        }

        // PUT: api/Department/5
        [HttpPut]
        [Authorize(Roles = "CanEditLocation")]
        public IActionResult EditLocation(EditLocation loc)
        {
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;

                }
                _service.EditLocation(loc.LocationId, loc.LocationName);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(new { message = ex });
            }
        }
    }
}
