﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Web.Data;
using Web.Data.Models;
using Web.Models;

namespace Web.Services
{
    public interface IOrderService
    {
        IEnumerable<Order> GetAllOrders();
        void CreateNewOrder(OrderMore orderProduct);
        void OrderedProduct(OrderedProduct orderedProduct);
    }

    public class OrderService : IOrderService
    {
        private readonly CovadisDbContext _Dbc;

        public OrderService(CovadisDbContext Dbc)
        {
            _Dbc = Dbc;
        }
        //Vraagt alle orders op uit de database.
        public IEnumerable<Order> GetAllOrders()
        {
            return _Dbc.TblOrders.Include(x => x.DepartmentId).Include(x => x.UserId).Include(x => x.ProductId).Include(x => x.StatusId);
        }
        //Maakt een Order aan en slaat dit op in de database.
        public void CreateNewOrder(OrderMore orderProduct)
        {

            var user = _Dbc.TblUsers.Where(p => p.UserId == orderProduct.UserId).FirstOrDefault();
            var department = _Dbc.TblDepartment.Where(p => p.DepartmentId == orderProduct.DepartmentId).FirstOrDefault();
            var product = _Dbc.TblProducts.Where(p => p.ProductId == orderProduct.Id).FirstOrDefault();
            var userDepartment = _Dbc.TblUserDepartment.Where(p => p.UserId == user).Where(l => l.DepartmentId == department).FirstOrDefault();
            var productDepartment = _Dbc.TblProductDepartment.Where(p => p.ProductId == product).Where(l => l.DepartmentId == department).FirstOrDefault();

            if (productDepartment != null)
            {
                if (userDepartment != null)
                {
                    if (product != null)
                    {
                        if (user != null)
                        {
                            if (department != null)
                            {
                                if (int.TryParse(orderProduct.Amount.ToString(), out int amount))
                                {
                                    var order = new Order
                                    {
                                        OrderAmount = amount,
                                        UserId = user,
                                        DepartmentId = department,
                                        ProductId = product,
                                        IsUrgent = orderProduct.Urgent,
                                        StatusId = _Dbc.TblStatus.Where(x => x.StatusName == "Openstaand").FirstOrDefault()
                                    };

                                    _Dbc.TblOrders.Add(order);
                                    _Dbc.SaveChanges();
                                }
                                else
                                {
                                    throw new Exception("U moet een getal invullen bij aantal!");
                                }
                            }
                            else
                            {
                                throw new Exception("De locatie bestaat niet!");
                            }
                        }
                        else
                        {
                            throw new Exception("De gebruiker bestaat niet!");
                        }
                    }
                    else
                    {
                        throw new Exception("Het product bestaat niet!");
                    }
                }
                else
                {
                    throw new Exception("U kan niet voor deze locatie bestellen!");
                }
            }
            else
            {
                throw new Exception("Dit product bestaat niet voor deze locatie");
            }
        }
        //Krijgt de status van een product die besteld is.
        public void OrderedProduct(OrderedProduct orderedProduct)
        {
            var user = _Dbc.TblUsers.Where(x => x.UserId == orderedProduct.UserId).FirstOrDefault();
            var order = _Dbc.TblOrders.Where(x => x.OrderId == orderedProduct.OrderId).Include(x => x.StatusId).FirstOrDefault();

            if(user != null)
            {
                if(order != null)
                {
                    var currentStatus = order.StatusId.StatusName;
                    Status status;

                    if(currentStatus == "Openstaand")
                    {
                        status = _Dbc.TblStatus.Where(x => x.StatusName == "Besteld").FirstOrDefault();
                        order.StatusId = status;
                        _Dbc.SaveChanges();
                    }
                    else if(currentStatus == "Besteld")
                    {
                        status = _Dbc.TblStatus.Where(x => x.StatusName == "Bezorgd").FirstOrDefault();
                        order.StatusId = status;
                        _Dbc.SaveChanges();
                    }
                    else
                    {
                        _Dbc.TblOrders.Remove(order);
                        _Dbc.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("De order bestaat niet!");
                }
            }
            else
            {
                throw new Exception("De gebruiker bestaat niet!");
            }
        }
    }
}
