﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Web.Data;
using Web.Data.Models;
using Web.Models;

namespace Web.Services
{
    public interface IRequestService
    {
        IEnumerable<Data.Models.RequestProduct> GetAllRequests();
        Data.Models.RequestProduct GetSpecificRequest(int id);
        void RequestNewProduct(Models.RequestProduct product);
        void AcceptRequest(Models.AcceptRequest product);
        void DenyRequest(int id);
    }

    public class RequestService : IRequestService
    {
        private readonly CovadisDbContext _Dbc;

        public RequestService(CovadisDbContext Dbc)
        {
            _Dbc = Dbc;
        }
        //
        public IEnumerable<Data.Models.RequestProduct> GetAllRequests()
        {
            return _Dbc.TblRequestProduct.Include(u => u.RequestedBy).Include(b => b.Location);
        }

        public Data.Models.RequestProduct GetSpecificRequest(int id)
        {
            return _Dbc.TblRequestProduct.Where(r => r.RequestProductId == id)
                                         .Include(u => u.RequestedBy)
                                         .Include(d => d.Location).FirstOrDefault();
        }

        public void RequestNewProduct(Models.RequestProduct product)
        {
            var user = _Dbc.TblUsers.Where(id => id.UserId == product.RequestedBy).FirstOrDefault();
            var department = _Dbc.TblDepartment.Where(id => id.DepartmentId == product.Location).FirstOrDefault();
            var userDepartment = _Dbc.TblUserDepartment.Where(x => x.UserId == user)
                                                        .Where(d => d.DepartmentId == department).FirstOrDefault();

            if(user != null)
            {
                if(department != null)
                {
                    if(userDepartment != null)
                    {
                        var newProduct = new Data.Models.RequestProduct
                        {
                            ProductName = product.ProductName,
                            Motivation = product.Motivation,
                            RequestedBy = user,
                            Location = department
                        };

                        _Dbc.TblRequestProduct.Add(newProduct);
                        _Dbc.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("De gebruiker is niet aan deze locatie gekoppeld!");
                    }
                }
                else
                {
                    throw new Exception("De locatie bestaat niet!");
                }
            }
            else
            {
                throw new Exception("De gebruiker bestaat niet!");
            }
        }

        public void AcceptRequest(AcceptRequest product)
        {
            var request = _Dbc.TblRequestProduct.Where(x => x.RequestProductId == product.RequestId).FirstOrDefault();
            var shop = _Dbc.TblShop.Where(x => x.ShopName == product.Shop).FirstOrDefault();

            if (shop == null)
            {
                var newShop = new Shop
                {
                    ShopName = product.Shop,
                    Inactive = false,
                };

                _Dbc.TblShop.Add(newShop);
                _Dbc.SaveChanges();
            }

            if (product.ProductId == 0)
            {
                var newProduct = new Data.Models.Product
                {
                    ProductImage = product.Image,
                    ProductName = product.Name,
                    ProductShop = _Dbc.TblShop.Where(x => x.ShopName == product.Shop).FirstOrDefault(),
                    ProductLink = product.Link,
                    ProductShortDurability = product.Durability
                };

                _Dbc.TblProducts.Add(newProduct);
                _Dbc.SaveChanges();

                var newLocationProduct = new Data.Models.ProductDepartment
                {
                    DepartmentId = _Dbc.TblDepartment.Where(x => x.DepartmentId == product.Location).FirstOrDefault(),
                    ProductId = _Dbc.TblProducts.Where(x => x.ProductName == product.Name).FirstOrDefault()
                };

                _Dbc.TblProductDepartment.Add(newLocationProduct);
                _Dbc.SaveChanges();
            }
            else
            {
                var newLocationProduct = new Data.Models.ProductDepartment
                {
                    DepartmentId = _Dbc.TblDepartment.Where(x => x.DepartmentId == product.Location).FirstOrDefault(),
                    ProductId = _Dbc.TblProducts.Where(x => x.ProductId == product.ProductId).FirstOrDefault()
                };

                _Dbc.TblProductDepartment.Add(newLocationProduct);
                _Dbc.SaveChanges();

            }

            if (_Dbc.TblRequestProduct.Where(x => x.ProductName == product.Name).Count() != 0)
            {
                _Dbc.TblRequestProduct.Remove(_Dbc.TblRequestProduct.Where(x => x.ProductName == product.Name).FirstOrDefault());
            }
            _Dbc.TblRequestProduct.Remove(request);
            _Dbc.SaveChanges();
        }

        public void DenyRequest(int id)
        {
            _Dbc.TblRequestProduct.Remove(_Dbc.TblRequestProduct.Where(x => x.RequestProductId == id).First());
            _Dbc.SaveChanges();
        }
    }
}
