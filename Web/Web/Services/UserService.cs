﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Web.Data;
using Web.Data.Models;
using Web.Helpers;
using Web.Models;

namespace Web.Services
{
    public interface IUserService
	{
		User Authenticate(string username, string password);
		IEnumerable<User> GetAllUsers();
        void RegisterUser(RegisterUser registerUser);
        User GetSpecificUser(int id);
        IEnumerable<Permissions> GetAllPermissions();
        IEnumerable<UserPermission> GetAllUserPermissions(int userId);
        void UpdateUserPermissions(NewUserPermissions permissions);
        void ChangePassword(ChangePassword password);
        void ConfirmPassword(ConfirmPassword password);
        IEnumerable<UserDepartment> GetAllUserDepartments();
        IEnumerable<UserDepartment> GetAllUserSpecificDepartments(int userId);
        IEnumerable<UserDepartment> GetAllLocationUsers(int departmentId);
        void NewLocationUser(NewLocationUser newUser);
        void DeleteUserLocation(int id);
        void ForgotPassword(string email);
        void DeleteUser(int id);
    }

	public class UserService : IUserService
	{
		private readonly AppSettings _appSettings;
		private readonly CovadisDbContext _Dbc;

		public UserService(IOptions<AppSettings> appSettings, CovadisDbContext Dbc)
		{
			_appSettings = appSettings.Value;
			_Dbc = Dbc;
		}
        //Dit is het Authenticatie systeem voor het inloggen.

        public User Authenticate(string username, string password)
        {
            var hashedPassword = Hash.Sha256(password);

            var user = _Dbc.TblUsers.Where(x => x.Username == username && x.Password == hashedPassword).FirstOrDefault();

            if (user != null)
            {
                int count = _Dbc.TblUserPermission.Where(x => x.UserId.UserId == user.UserId).Count();
                var claims = new Claim[count];
                var claimNames = _Dbc.TblUserPermission.Where(x => x.UserId.UserId == user.UserId).Include(x => x.PermissionId).ToArray();
                for (int i = 0; i < count; i++)
                {
                    claims[i] = new Claim(ClaimTypes.Role, claimNames[i].PermissionId.Permission);
                }

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                user.Token = tokenHandler.WriteToken(token);
                user.Password = null;
                user.Permissions = new string[count];
                for (int i = 0; i < count; i++)
                {
                    user.Permissions[i] = claimNames[i].PermissionId.Permission;
                }

                return user;
            }
            else
            {
                throw new Exception("Incorrecte gebruikersnaam of wachtwoord");
            }
		}

        //Alle gebruikers worden opgehaalt.
        public IEnumerable<User> GetAllUsers()
		{
            int count = _Dbc.TblUsers.Where(x => x.Inactive == false).Count();
            User[] dbUsers = _Dbc.TblUsers.Where(x => x.Inactive == false).ToArray();
            User[] users = new User[count];
            for (int i = 0; i < count; i++)
            {
                users[i] = dbUsers[i];
                dbUsers[i].Password = null;
            }
            return users;
		}
        //Een specifiek gebruiker wordt opgehaalt.
        public User GetSpecificUser(int id)
        {
            var user = _Dbc.TblUsers.Where(x => x.UserId == id).FirstOrDefault();
            if (user != null)
            {
                user.Password = null;
                return user;
            }
            else
            {
                throw new Exception("Er is geen gebruiker met dit Id!");
            }
        }

        // Een method voor het registreren van een user.
        public void RegisterUser(RegisterUser registerUser)
        {
            if(_Dbc.TblUsers.Where(x => x.Username == registerUser.UserName).Count() == 0)
            {
                if(registerUser.UserName == "" || registerUser.LastName == "" || registerUser.FirstName == "")
                {
                    var user = new User
                    {
                        FirstName = registerUser.FirstName,
                        LastName = registerUser.LastName,
                        Username = registerUser.UserName
                    };

                    _Dbc.TblUsers.Add(user);
                    _Dbc.SaveChanges();

                    var PasswordConfirm = new UserPasswordConfirms
                    {
                        UserId = user,
                        Guid = Guid.NewGuid()
                    };

                    _Dbc.TblUserPasswordConfirms.Add(PasswordConfirm);
                    _Dbc.SaveChanges();

                    SendEmail email = new SendEmail();

                    email.SendMail(user, "ConfirmPassword", PasswordConfirm.Guid);
                }
                else
                {
                    throw new Exception("Alle waardes moeten ingevuld worden!");
                }
            }
            else
            {
                throw new Exception("Een gebruiker met deze gebruikersnaam bestaat al!");
            }
        }

        //Alle permissies worden opgehaalt
        public IEnumerable<Permissions> GetAllPermissions()
        {
            return _Dbc.TblPermissions;
        }
        //Alle permissies van een bepaalde gebruiker worden opgehaalt
        public IEnumerable<UserPermission> GetAllUserPermissions(int userId)
        {
            var user = _Dbc.TblUsers.Where(x => x.UserId == userId).FirstOrDefault();
            if(user != null)
            {
                return _Dbc.TblUserPermission.Where(x => x.UserId == user).Include(x => x.UserId).Include(x => x.PermissionId);
            }
            else
            {
                throw new Exception("Deze gebruiker kan niet gevonden worden!");
            }
        }
        //Een permissie wordt toegevoegt aan een gebruiker
        public void UpdateUserPermissions(NewUserPermissions permissions)
        {
            var user = _Dbc.TblUsers.Where(x => x.UserId == permissions.UserId).FirstOrDefault();

            if(user != null)
            {
                int oldPermissionsCount = _Dbc.TblUserPermission.Where(x => x.UserId == user).ToArray().Length;
                string[] oldPermissions = new string[oldPermissionsCount];

                for (int i = 0; i < oldPermissionsCount; i++)
                {
                    oldPermissions[i] = _Dbc.TblUserPermission.Where(x => x.UserId == user).Include(x => x.PermissionId).First().PermissionId.Permission;
                }

                var newPermissions = permissions.NewPermissions;
                newPermissions = newPermissions.Where(x => x != null).ToArray();

                foreach (var perm in oldPermissions)
                {
                    _Dbc.TblUserPermission.Remove(_Dbc.TblUserPermission.Where(x => x.UserId == user).First());
                    _Dbc.SaveChanges();
                }
                foreach (var newPerm in newPermissions)
                {
                    var newPermission = new UserPermission
                    {
                        PermissionId = _Dbc.TblPermissions.Where(x => x.Permission == newPerm.ToString()).FirstOrDefault(),
                        UserId = user
                    };

                    _Dbc.TblUserPermission.Add(newPermission);
                    _Dbc.SaveChanges();
                }
            }
            else
            {
                throw new Exception("Deze gebruiker bestaat niet!");
            }
        }

        //Dit is een method voor het Veranderen van een wachtwoord
        public void ChangePassword(ChangePassword password)
        {
            var user = _Dbc.TblUsers.Where(x => x.UserId == password.UserId).FirstOrDefault();

            if(user != null) {
                var oldPassword = user.Password;
                if (Hash.Sha256(password.OldPassword) == oldPassword)
                {
                    if (password.NewPassword == password.NewPasswordConfirm)
                    {
                        if (password.NewPassword.Length >= 8 || 
                            password.NewPassword.Any(char.IsDigit) || 
                            password.NewPassword.Any(char.IsLower) || 
                            password.NewPassword.Any(char.IsUpper))
                        {
                            _Dbc.TblUsers.Where(x => x.UserId == user.UserId)
                                         .FirstOrDefault().Password = Hash.Sha256(password.NewPassword);
                            _Dbc.SaveChanges();
                        }
                        else
                        {
                            throw new Exception("Uw wachtwoord op zijn minst 8 karakters lang zijn, een getal, een hoofdletter en een kleine letter bezitten!");
                        }
                    }
                    else
                    {
                        throw new Exception("De nieuwe wachtwoorden komen niet overeen!");
                    }
                }
                else
                {
                    throw new Exception("Het oude wachtwoord komt niet overeen met het ingevulde wachtwoord");
                }
            }
            else
            {
                throw new Exception("Deze gebruiker bestaat niet!");
            }
        }
        //Dit is de method voor het Activeren van een gebruikers wachtwoord.
        public void ConfirmPassword(ConfirmPassword password)
        {
            Guid convertedGuid = new Guid(password.Guid);

            var user = _Dbc.TblUserPasswordConfirms.Where(x => x.Guid == convertedGuid).Include(x => x.UserId).FirstOrDefault();

            if (user != null)
            {
                if (password.Password == password.PasswordConfirm)
                {
                    if (password.Password.Any(char.IsDigit) || 
                        password.Password.Any(char.IsUpper) || 
                        password.Password.Any(char.IsLower) || 
                        password.Password.Length >= 8)
                    {
                        _Dbc.TblUsers.Where(x => x.UserId == user.UserId.UserId).FirstOrDefault().Password = Hash.Sha256(password.Password);
                        _Dbc.SaveChanges();

                        _Dbc.TblUserPasswordConfirms.Remove(_Dbc.TblUserPasswordConfirms.Where(x => x.Guid == convertedGuid).First());
                        _Dbc.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("Uw wachtwoord op zijn minst 8 karakters lang zijn, een getal, een hoofdletter en een kleine letter bezitten!");
                    }
                }
                else
                {
                    throw new Exception("De wachtwoorden komen niet overeen");
                }
            }
            else
            {
                throw new Exception("U kan uw wachtwoord niet bevestigen");
            }
        }
        //Alle locties worden opgehaald
        public IEnumerable<UserDepartment> GetAllUserDepartments()
        {
            return _Dbc.TblUserDepartment.Include(u => u.UserId).Include(d => d.DepartmentId);
        }
        //De gebruikers worden toegevoegt aan een locatie
        public IEnumerable<UserDepartment> GetAllUserSpecificDepartments(int userId)
        {
            var user = _Dbc.TblUsers.Where(x => x.UserId == userId).FirstOrDefault();
            if(user != null)
            {
                return _Dbc.TblUserDepartment.Where(b => b.UserId == user).Include(d => d.DepartmentId).Include(u => u.UserId);
            }
            else
            {
                throw new Exception("Deze gebruiker bestaat niet!");
            }
        }
        //Er wordt een locatie Aangemaakt
        public IEnumerable<UserDepartment> GetAllLocationUsers(int departmentId)
        {
            var department = _Dbc.TblDepartment.Where(x => x.DepartmentId == departmentId).FirstOrDefault();

            if(department != null)
            {
                return _Dbc.TblUserDepartment.Where(x => x.DepartmentId == department).Include(d => d.DepartmentId).Include(u => u.UserId);
            }
            else
            {
                throw new Exception("Deze locatie bestaat niet!");
            }
        }

        public void NewLocationUser(NewLocationUser newUser)
        {
            var user = _Dbc.TblUsers.Where(x => x.UserId == newUser.UserId).FirstOrDefault();
            var department = _Dbc.TblDepartment.Where(x => x.DepartmentId == newUser.DepartmentId).First();

            if(user != null)
            {
                if(department != null)
                {
                    var locationUser = new UserDepartment
                    {
                        DepartmentId = department,
                        UserId = user
                    };

                    _Dbc.TblUserDepartment.Add(locationUser);
                    _Dbc.SaveChanges();
                }
                else
                {
                    throw new Exception("Deze locatie bestaat niet!");
                }
            }
            else
            {
                throw new Exception("Deze gebruiker bestaat niet!");
            }
        }
        //De Gebruiker wordt van de locatie verwijderd
        public void DeleteUserLocation(int id)
        {
            var location = _Dbc.TblUserDepartment.Where(x => x.UserDepartmentId == id).FirstOrDefault();

            if (location != null)
            {
                _Dbc.TblUserDepartment.Remove(location);
                _Dbc.SaveChanges();
            }
            else
            {
                throw new Exception("Deze locatie kan niet gevonden worden!");
            }
        }
        //Deze method is voor het vergeten van een wachtwoord bij de gebruiker.
        public void ForgotPassword(string email)
        {
            var user = _Dbc.TblUsers.Where(x => x.Username == email).FirstOrDefault();
            var passwordRequests = _Dbc.TblUserPasswordConfirms.Where(x => x.UserId == user).Count();

            if(user != null)
            {
                if(passwordRequests == 0)
                {
                    var passwordConfirm = new UserPasswordConfirms
                    {
                        UserId = user,
                        Guid = Guid.NewGuid()
                    };

                    _Dbc.TblUserPasswordConfirms.Add(passwordConfirm);
                    _Dbc.SaveChanges();

                    var guid = _Dbc.TblUserPasswordConfirms.Where(x => x.UserId == user).FirstOrDefault().Guid;

                    var sendEmail = new SendEmail();
                    sendEmail.SendMail(user, "ForgotPassword", guid);
                }
                else
                {
                    throw new Exception("U heeft al een nieuw wachtwoord aangevraagd!");
                }
            }
            else
            {
                throw new Exception("Dit account kan niet gevonden worden!");
            }
        }

        public void DeleteUser(int id)
        {
            var user = _Dbc.TblUsers.Where(x => x.UserId == id).FirstOrDefault();

            user.Inactive = true;

            var userLocations = _Dbc.TblUserDepartment.Where(x => x.UserId == user);

            _Dbc.TblUserDepartment.RemoveRange(userLocations);
            _Dbc.SaveChanges();
        }
    }
}
