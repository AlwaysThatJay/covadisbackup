﻿using System;
using System.Collections.Generic;
using System.Linq;
using Web.Data;
using Web.Data.Models;
using Web.Models;

namespace Web.Services
{
    public interface IDepartmentService
    {
        IEnumerable<Department> GetAllDepartments();
        Department GetSpecificDepartment(int id);
        void NewLocation(AddLocation addlocation);
        Department EditLocation(int locationId, string locationName);
    }

    public class DepartmentService : IDepartmentService
    {
        private readonly CovadisDbContext _Dbc;

        public DepartmentService(CovadisDbContext Dbc)
        {
            _Dbc = Dbc;
        }
        //Haalt alle locaties op vanuit de database.
        public IEnumerable<Department> GetAllDepartments()
        {
            return _Dbc.TblDepartment;
        }
        //Haalt een specifieke locatie op vanuit de database.
        public Department GetSpecificDepartment(int id)
        {
            return _Dbc.TblDepartment.Where(b => b.DepartmentId == id).FirstOrDefault();
        }
        //Een nieuwe locatie aanmaken, en word toegevoegd in de database.
        public void NewLocation(AddLocation addlocation)
        {
            if (_Dbc.TblDepartment.Where(d => d.DepartmentName == addlocation.LocationName).Count() == 0)
            {
                if (addlocation.LocationName != "")
                {
                    var department = new Department
                    {
                        DepartmentName = addlocation.LocationName
                    };

                    _Dbc.TblDepartment.Add(department);
                    _Dbc.SaveChanges();
                }
                else
                {
                    throw new Exception("Deze locatie bestaat al!");
                }
            }
            else
            {
                throw new Exception("Vul een naam in voor de nieuwe locatie!");
            }
        }
        //Als je de naam van de locatie wil veranderen.
        public Department EditLocation(int locationId, string locationName)
        {
            var location = _Dbc.TblDepartment.Where(x => x.DepartmentId == locationId).FirstOrDefault();

            if (location == null)
            {
                throw new Exception("Deze locatie bestaat niet!");
            }
            else
            {
                if (locationName != "")
                {
                    location.DepartmentName = locationName;
                    _Dbc.SaveChanges();

                    return location;
                }
                else
                {
                    throw new Exception("Vul een nieuwe naam in voor de locatie!");
                }
            }
        }
    }
}
