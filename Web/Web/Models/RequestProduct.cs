﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class RequestProduct
    {
        public string ProductName { get; set; }
        public string Motivation { get; set; }
        public int RequestedBy { get; set; }
        public int Location { get; set; }
    }
}
