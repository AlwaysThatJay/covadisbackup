﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class NewLocationProduct
    {
        public int DepartmentId { get; set; }
        public int ProductId { get; set; }
    }
}
