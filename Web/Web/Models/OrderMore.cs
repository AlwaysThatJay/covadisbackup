﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class OrderMore
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public bool Urgent { get; set; }
        public int DepartmentId { get; set; }
        public int UserId { get; set; }
    }
}
