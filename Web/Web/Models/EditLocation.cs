﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class EditLocation
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }
}
