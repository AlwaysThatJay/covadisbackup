﻿namespace Web.Models
{
    public class AcceptRequest
    {
        public int RequestId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Shop { get; set; }
        public string Link { get; set; }
        public bool Durability { get; set; }
        public int Location { get; set; }
        public int ProductId { get; set; }
    }
}