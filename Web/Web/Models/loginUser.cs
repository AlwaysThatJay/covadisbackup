﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
	public class LoginUser
	{
		public string userName { get; set; }
		public string password { get; set; }
	}
}
