﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class OrderedProduct
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
    }
}
