﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class NewUserPermissions
    {
        public int UserId { get; set; }
        public string[] NewPermissions { get; set; }
    }
}
