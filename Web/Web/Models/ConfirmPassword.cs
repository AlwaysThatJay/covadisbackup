﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class ConfirmPassword
    {
        public string Guid { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
    }
}
